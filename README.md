# python3-examples

Examples, Demos and Tools for Python. These are files included in the upstream Python distribution. https://python.org

Example: https://gitlab.com/debian-packages-demo/clang (CI/CD string job)

## From source
* https://github.com/python/cpython/blob/master/Tools/scripts/README

## diff.py ([difflib](https://docs.python.org/3.7/library/difflib.html))
* Example: https://gitlab.com/debian-packages-demo/clang (string CI/CD job)
* https://github.com/python/cpython/blob/master/Tools/scripts/diff.py
* TODO separate libraries and their cli!

### Similar packages
#### Alpine
Not yet available 3.9
* https://pkgs.alpinelinux.org/contents?file=diff.py&arch=x86_64

#### PHP
* https://phppackages.org/p/sebastian/diff (apparently no html output)
* https://phppackages.org/p/phpspec/php-diff (by Chris Boulton)
* https://phppackages.org/p/neos/diff (less recomanded)
* https://phppackages.org/s/diff

#### Javascript
* https://gitlab.com/javascript-packages-demo/diff2html-cli
